# GTRLabs Database #

Provides an object layer and database of information sourced from the NIH about the individual
labs that provide genetic tests.

The GTR archive downloadable from the NIH contains only genetic testing information, not the information 
it contains on the individual labs and their specific testing information.  Thus this code is supplementary.
