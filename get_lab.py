# Load single lab and parse out its details from the page.

import os
import logging

import requests
from lxml.html import HTMLParser
from lxml import etree


LAB_SOURCE_URI_TMPL = 'https://www.ncbi.nlm.nih.gov/gtr/labs/%i' # e.g. 29669, 242449
DATADIR = './data'
FILENAME_TMPL = 'GTRLab_%i.html'

log = logging.getLogger()
fh = logging.FileHandler('/tmp/GTRLab.log')
log.addHandler(fh)
log.setLevel(logging.DEBUG)


class GTRLabDataNotFound(Exception):
    pass


def _gtr_lab_filename(lab_id):
    return os.path.join(DATADIR, FILENAME_TMPL % lab_id)

def load_GTR_lab_page(lab_id):
    uri = LAB_SOURCE_URI_TMPL % lab_id
    result = requests.get(uri)
    if result.ok:
        return result.text
    else:
        raise Exception(result.status_code, uri)


class GTRLab(object):

    def __init__(self, lab_id, source, **kwargs):
        self.id = int(lab_id)
        self._source = source

    @classmethod
    def from_GTR(cls, lab_id):
        source = load_GTR_lab_page(int(lab_id)) 
        log.debug('Loaded GTRLab %s from web', str(lab_id))
        return cls(lab_id, source)

    @classmethod
    def from_cache(cls, lab_id):
        fname = _gtr_lab_filename(lab_id)
        if os.path.exists(fname):
            log.debug('Loaded GTRLab %s from cache', str(lab_id))
            return cls(lab_id, open(fname, 'r').read())
        raise GTRLabDataNotFound('No file at %s' % fname)

    @property
    def name(self):
        """ Return name of lab. """
        
    @property
    def location(self):
        """ Returns general location (e.g. city, state, country) of lab. """

    @property
    def CLIA(self):
        """ Returns CLIA certification status of lab. """
    
    @property
    def phone(self):
        """ Returns phone number of lab. """

    @property
    def tests(self):
        """ Returns list of tests offered by this lab. """

    def write_source(self):
        """ Writes source html to file (directory and filename specified in config). """
        fh = open(_gtr_lab_filename(self.id), 'w')
        fh.write(self._source)
        fh.close()
        log.debug('Wrote GTRLab %s to cache', str(self.id))


def main(idlist):
    for lab_id in idlist:
        try:
            lab = GTRLab.from_cache(lab_id)
        except GTRLabDataNotFound:
            lab = GTRLab.from_GTR(lab_id)
            lab.write_source()
        except Exception as error:
            print(error)


if __name__=='__main__':
    #main([242449, 1012, 230769, 505330])

    main(list(range(1012)))


